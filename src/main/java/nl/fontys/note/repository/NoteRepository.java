package nl.fontys.note.repository;

import nl.fontys.note.models.Note;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface NoteRepository extends JpaRepository<Note, Long> {
//    @Query("select c from Note c " +
//            "where lower(c.supplier.id) like lower(concat('%', :searchTerm, '%')) ")
    @Query("select c from Note c where c.supplier.uuid = :id ")
    List<Note> findAllbyParantIdSupplier(@Param("id") String parentId);

    @Query("select c from Note c where c.customer.uuid = :id ")
    List<Note> findAllbyParantIdCustomer(@Param("id") String parentId);
}
