package nl.fontys.note.repository;
import java.util.List;

import nl.fontys.note.models.Supplier;
import org.springframework.data.jpa.repository.JpaRepository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface SupplierRepository extends JpaRepository<Supplier, Long>{
    //List<Customer> findByActive(boolean active);
    //List<Customer> findByCompanyName(String companyName);

    @Query("select c from Supplier c " +
            "where lower(c.companyName) like lower(concat('%', :searchTerm, '%')) ")
    List<Supplier> findByCompanyName(@Param("searchTerm") String companyName);

    Supplier findByUuid(String uuid);

    void deleteByUuid(String supplierUuid);

//    @Query("select c from Supplier c " +
//            "where lower(c.status) like lower(concat('%', :status, '%')) ")
//    List<Supplier> findByActive(@Param("status") Enum status);
}
