package nl.fontys.note.repository;
import java.util.List;

import nl.fontys.note.models.Customer;
import org.springframework.data.jpa.repository.JpaRepository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface CustomerRepository extends JpaRepository<Customer, Long>{
    //List<Customer> findByActive(boolean active);
    //List<Customer> findByCompanyName(String companyName);

    @Query("select c from Customer c " +
            "where lower(c.companyName) like lower(concat('%', :searchTerm, '%')) ")
    List<Customer> findByCompanyName(@Param("searchTerm") String companyName);

//    @Query("select c from Customer c " +
//            "where lower(c.status) like lower(concat('%', :active, '%')) ")
//    List<Customer> findByActive(@Param("active") boolean active);

     Customer findByUuid(String uuid);

//    @Query("select c from Customer c where c.uuid = (concat('%', :uuid, '%')) ")
//    Customer findByUuid(@Param("uuid") String uuid);

    void deleteByUuid(String customerUuid);

//    @Query("select c from Supplier c " +
//            "where lower(c.status) like lower(concat('%', :status, '%')) ")
//    List<Supplier> findByActive(@Param("status") Enum status);
}
