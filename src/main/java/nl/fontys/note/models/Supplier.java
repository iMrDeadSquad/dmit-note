package nl.fontys.note.models;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.json.JSONObject;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;

@Entity
@Getter
@Setter
@NoArgsConstructor
public class Supplier{
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @NotBlank
    private String companyName;

    @Column(name = "supplier_uuid")
    private String uuid;

    public Supplier(String companyName) {
        this.companyName = companyName;
    }

    public Supplier(JSONObject jsonObject) {
        this.companyName = jsonObject.getString("companyName");
        this.uuid = jsonObject.getString("uuid");
    }
}
