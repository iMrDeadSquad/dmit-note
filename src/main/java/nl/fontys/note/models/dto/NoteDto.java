package nl.fontys.note.models.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class NoteDto {
    long id;
    String supplierId;
    String customerId;
    String uuid;
    String content;
}
