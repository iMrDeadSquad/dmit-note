package nl.fontys.note.models;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.json.JSONObject;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;

@Entity
@Getter
@Setter
@NoArgsConstructor
public class Customer{
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @NotBlank
    private String companyName;

    @Column(name = "customer_uuid")
    private String uuid;

    public Customer(String companyName) {
        this.companyName = companyName;
    }

    public Customer(JSONObject jsonObject) {
        this.companyName = jsonObject.getString("companyName");
        this.uuid = jsonObject.getString("uuid");
    }
}
