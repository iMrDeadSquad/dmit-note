package nl.fontys.note.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;
import java.util.Optional;

@Entity
@Getter
@Setter
@NoArgsConstructor
public class Note implements Cloneable{

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "supplierId")
    private Supplier supplier;

    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "customerId")
    private Customer customer;


    private String Content= "";

    @Temporal(TemporalType.DATE)
    private Date noteDate = new Date();



    public Note(Supplier supplier, String content, Date noteDate) {
        this.supplier = supplier;
        this.Content = content;
        this.noteDate = noteDate;
    }
    public Note(Customer customer, String content, Date noteDate) {
        this.customer = customer;
        this.Content = content;
        this.noteDate = noteDate;
    }
}
