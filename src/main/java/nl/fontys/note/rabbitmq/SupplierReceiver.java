package nl.fontys.note.rabbitmq;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.DeliverCallback;
//import io.github.cdimascio.dotenv.Dotenv;
import nl.fontys.note.models.Supplier;
import nl.fontys.note.repository.SupplierRepository;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.concurrent.TimeoutException;

@Component
public class SupplierReceiver {

    private static SupplierRepository supplierRepository;

    //private static final Dotenv env = Dotenv.load();

    @Value("${RABBITMQ_HOST}")
    private static String host;

    @Value("${RABBITMQ_DEFAULT_USER}")
    private static String user;

    @Value("${RABBITMQ_DEFAULT_PASS}")
    private static String pass;

    private static final String EXCHANGE_NAME = "supplier_exchange";

    private static Channel channel;

    static {
        try {
            if (channel == null) {
                channel = getFactory().newConnection().createChannel();
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (TimeoutException e) {
            e.printStackTrace();
        }
    }

    public SupplierReceiver(SupplierRepository supplierRepository) {
        this.supplierRepository = supplierRepository;
    }

    private static ConnectionFactory getFactory() {
        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost("localhost");
        factory.setUsername("guest");
        factory.setPassword("guest");
        return factory;
    }


//    private static ConnectionFactory getFactory() {
//        ConnectionFactory factory = new ConnectionFactory();
//        factory.setHost("rabbitmq");
//        factory.setUsername("admin");
//        factory.setPassword("root");
//        return factory;
//    }

    public static void init() throws IOException {
        receive("supplier.add", createSupplier);
        receive("supplier.delete", deleteSupplier);
        receive("supplier.update", updateSupplier);
    }

    private static void receive(String key, DeliverCallback deliverCallback) throws IOException {
        channel.exchangeDeclare(EXCHANGE_NAME, "topic");
        String queueName = channel.queueDeclare().getQueue();

        channel.queueBind(queueName, EXCHANGE_NAME, key);
        // doesn't automatically acknowledge the message when it is received
        boolean autoAck = false;
        channel.basicConsume(queueName, autoAck, deliverCallback, consumerTag -> { });
    }
    private static DeliverCallback createSupplier = (consumerTag, delivery) -> {
        try {
            String data = new String(delivery.getBody(), StandardCharsets.UTF_8);
            try {
                System.out.println(data);
                JSONObject jsonObject = new JSONObject(data);
                Supplier supplier = new Supplier(jsonObject);
                supplierRepository.save(supplier);
            }catch (JSONException err){
                System.out.println("Error" + err.toString());
            }
            System.out.println(" [x] Supplier received from RabbitMQ");
        } finally {
            channel.basicAck(delivery.getEnvelope().getDeliveryTag(), false);
            System.out.println(" [x] Done");
        }
    };

    private static DeliverCallback deleteSupplier = (consumerTag, delivery) -> {
        try {
            String data = new String(delivery.getBody(), StandardCharsets.UTF_8);
            try {
                System.out.println(data);
                JSONObject jsonObject = new JSONObject(data);
                Supplier supplier = new Supplier(jsonObject);
                Supplier supplierLocal = supplierRepository.findByUuid(supplier.getUuid());
                supplierRepository.deleteById(supplierLocal.getId());
            }catch (JSONException err){
                System.out.println("Error" + err.toString());
            }
            System.out.println(" [x] Supplier removed via RabbitMQ ");
        } finally {
            channel.basicAck(delivery.getEnvelope().getDeliveryTag(), false);
            System.out.println(" [x] Done");
        }
    };

    private static DeliverCallback updateSupplier = (consumerTag, delivery) -> {
        try {
            String data = new String(delivery.getBody(), StandardCharsets.UTF_8);
            try {
                System.out.println(data);
                JSONObject jsonObject = new JSONObject(data);
                Supplier supplier = new Supplier(jsonObject);
                Supplier supplierData = supplierRepository.findByUuid(supplier.getUuid());
                if (supplierData != null) {
                    Supplier _supplier = supplierData;
                    _supplier.setCompanyName(supplier.getCompanyName());
                    supplierRepository.save(_supplier);
                }
            }catch (JSONException err){
                System.out.println("Error" + err.toString());
            }
            System.out.println(" [x] Supplier updated via RabbitMQ ");
        } finally {
            channel.basicAck(delivery.getEnvelope().getDeliveryTag(), false);
            System.out.println(" [x] Done");
        }
    };
}
