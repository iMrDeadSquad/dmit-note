package nl.fontys.note.rabbitmq;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.DeliverCallback;
//import io.github.cdimascio.dotenv.Dotenv;
import nl.fontys.note.models.Customer;
import nl.fontys.note.repository.CustomerRepository;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.concurrent.TimeoutException;

@Component
public class CustomerReceiver {

    private static CustomerRepository customerRepository;

    //private static final Dotenv env = Dotenv.load();

    private static final String EXCHANGE_NAME = "customer_exchange";

    private static Channel channel;

    static {
        try {
            if (channel == null) {
                channel = getFactory().newConnection().createChannel();
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (TimeoutException e) {
            e.printStackTrace();
        }
    }

    public CustomerReceiver(CustomerRepository customerRepository) {
        this.customerRepository = customerRepository;
    }

    private static ConnectionFactory getFactory() {
        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost("localhost");
        factory.setUsername("guest");
        factory.setPassword("guest");
        return factory;
    }


//    private static ConnectionFactory getFactory() {
//        ConnectionFactory factory = new ConnectionFactory();
//        factory.setHost("rabbitmq");
//        factory.setUsername("admin");
//        factory.setPassword("root");
//        return factory;
//    }


    public static void init() throws IOException {
        receive("customer.add", createCustomer);
        receive("customer.delete", deleteCustomer);
        receive("customer.update", updateCustomer);
    }

    private static void receive(String key, DeliverCallback deliverCallback) throws IOException {
        channel.exchangeDeclare(EXCHANGE_NAME, "topic");
        String queueName = channel.queueDeclare().getQueue();

        channel.queueBind(queueName, EXCHANGE_NAME, key);
        // doesn't automatically acknowledge the message when it is received
        boolean autoAck = false;
        channel.basicConsume(queueName, autoAck, deliverCallback, consumerTag -> { });
    }
    private static DeliverCallback createCustomer = (consumerTag, delivery) -> {
        try {
            String data = new String(delivery.getBody(), StandardCharsets.UTF_8);
            try {
                System.out.println(data);
                JSONObject jsonObject = new JSONObject(data);
                Customer customer = new Customer(jsonObject);
                customerRepository.save(customer);
            }catch (JSONException err){
                System.out.println("Error" + err.toString());
            }
            System.out.println(" [x] Customer received from RabbitMQ");
        } finally {
            channel.basicAck(delivery.getEnvelope().getDeliveryTag(), false);
            System.out.println(" [x] Done");
        }
    };

    private static DeliverCallback deleteCustomer = (consumerTag, delivery) -> {
        try {
            String data = new String(delivery.getBody(), StandardCharsets.UTF_8);
            try {
                System.out.println(data);
                JSONObject jsonObject = new JSONObject(data);
                Customer customer = new Customer(jsonObject);
                Customer customerLocal = customerRepository.findByUuid(customer.getUuid());
                customerRepository.deleteById(customerLocal.getId());
            }catch (JSONException err){
                System.out.println("Error" + err.toString());
            }
            System.out.println(" [x] Customer removed via RabbitMQ ");
        } finally {
            channel.basicAck(delivery.getEnvelope().getDeliveryTag(), false);
            System.out.println(" [x] Done");
        }
    };

    private static DeliverCallback updateCustomer = (consumerTag, delivery) -> {
        try {
            String data = new String(delivery.getBody(), StandardCharsets.UTF_8);
            try {
                System.out.println(data);
                JSONObject jsonObject = new JSONObject(data);
                Customer customer = new Customer(jsonObject);
                Customer customerData = customerRepository.findByUuid(customer.getUuid());
                if (customerData != null) {
                    Customer _customer = customerData;
                    _customer.setCompanyName(customer.getCompanyName());
                    customerRepository.save(_customer);
                }
            }catch (JSONException err){
                System.out.println("Error" + err.toString());
            }
            System.out.println(" [x] Customer updated via RabbitMQ ");
        } finally {
            channel.basicAck(delivery.getEnvelope().getDeliveryTag(), false);
            System.out.println(" [x] Done");
        }
    };
}
