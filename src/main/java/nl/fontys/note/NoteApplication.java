package nl.fontys.note;

import nl.fontys.note.models.Supplier;
import nl.fontys.note.rabbitmq.CustomerReceiver;
import nl.fontys.note.rabbitmq.SupplierReceiver;
import nl.fontys.note.repository.SupplierRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class NoteApplication implements CommandLineRunner {

	@Autowired
	private SupplierRepository supplierRepository;

	public static void main(String[] args) {
		SpringApplication.run(NoteApplication.class, args);

	}

	@Override
	public void run(String... args) throws Exception {
		SupplierReceiver.init();
		CustomerReceiver.init();
//		Supplier supplier = new Supplier("Fontys");
//
//		supplierRepository.save(supplier);
	}
}
