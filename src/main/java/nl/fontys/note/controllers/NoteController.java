package nl.fontys.note.controllers;

import nl.fontys.note.models.Customer;
import nl.fontys.note.models.Note;
import nl.fontys.note.models.Supplier;
import nl.fontys.note.models.dto.NoteDto;
import nl.fontys.note.repository.CustomerRepository;
import nl.fontys.note.repository.NoteRepository;
import nl.fontys.note.repository.SupplierRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
@RequestMapping("/api")

public class NoteController {

    @Autowired
    SupplierRepository supplierRepository;

    @Autowired
    CustomerRepository customerRepository;

    @Autowired
    NoteRepository noteRepository;

    @GetMapping("/notes/{id}")
    public ResponseEntity<List<Note>> getAllNotesFromSupplier(@PathVariable("id") String parentId) {
        try {
            List<Note> notes = new ArrayList<>();
            noteRepository.findAllbyParantIdSupplier(parentId).forEach(notes::add);
            if (notes.isEmpty()) {
                noteRepository.findAllbyParantIdCustomer(parentId).forEach(notes::add);
            }
            else if (notes.isEmpty()) {
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            }
            return new ResponseEntity<>(notes, HttpStatus.OK);
        }   catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

//    @GetMapping("/notes/{id}")
//    public ResponseEntity<Note> getNoteById(@PathVariable("id") long id) {
//        Optional<Note> noteData = noteRepository.findById(id);
//
//        if (noteData.isPresent()) {
//            return new ResponseEntity<>(noteData.get(), HttpStatus.OK);
//        }
//        else {
//            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
//        }
//    }

//    @PostMapping("/notes")
//    public ResponseEntity<Note> createNote(@RequestBody NoteDto noteDto) {
//        System.out.println(noteDto);
//        Note note = new Note();
//        String supplierId = noteDto.getSupplierId();
//        //L64 nog even naar kijken
//        Supplier supplier = supplierRepository.findByUuid(supplierId); //NOSONAR
//        note.setSupplier(supplier);
//        note.setContent(noteDto.getContent());
//        note.setNoteDate(new java.util.Date());
//        try {
//            Note _note = noteRepository
//                    .save(new Note(note.getSupplier(), note.getContent(), note.getNoteDate()));
//            return new ResponseEntity<>(_note, HttpStatus.CREATED);
//        } catch (Exception e) {
//            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
//        }
//    }

    @PostMapping("/notes")
    public ResponseEntity<Note> createNote(@RequestBody NoteDto noteDto) {
//        System.out.println(noteDto.getUuid());
//        System.out.println(noteDto.getContent());
        Note note = new Note();
        String uuid = noteDto.getUuid();
//        if (uuid == null) {
//            uuid = noteDto.getCustomerId();
//        }
        //L64 nog even naar kijken
        Supplier supplier = null;
        Customer customer = null;
        System.out.println(uuid);
        if (uuid != null) {
            supplier = supplierRepository.findByUuid(uuid);
            if (supplier != null) {
                note.setSupplier(supplier);
            }
            else if (supplier == null) {
                customer = customerRepository.findByUuid(uuid);
                note.setCustomer(customer);
            }
            note.setContent(noteDto.getContent());
            note.setNoteDate(new java.util.Date());
        }
        try {
            if (supplier != null){
                Note _note = noteRepository
                        .save(new Note(note.getSupplier(), note.getContent(), note.getNoteDate()));
                System.out.println("Note saved for supplier");
                return new ResponseEntity<>(_note, HttpStatus.CREATED);
            }
            else if (customer != null){
                Note _note = noteRepository
                        .save(new Note(note.getCustomer(), note.getContent(), note.getNoteDate()));
                System.out.println("Note saved for customer");
                return new ResponseEntity<>(_note, HttpStatus.CREATED);
            }
            else {
                return new ResponseEntity<>( HttpStatus.BAD_REQUEST);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }

//    @PutMapping("/notes/{id}")
//    public ResponseEntity<Note> updateNote(@PathVariable("id") long id, @RequestBody Note note) {
//        Optional <Note> noteData = noteRepository.findById(id);
//
//        if (noteData.isPresent()) {
//            Note _note = noteData.get();
//            _note.setSupplier(note.getSupplier());
//            _note.setContent(note.getContent());
//            _note.setNoteDate(note.getNoteDate());
//            return new ResponseEntity<>(noteRepository.save(_note), HttpStatus.OK);
//        } else {
//            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
//        }
//    }
    @DeleteMapping("/notes/{id}")
    public ResponseEntity<HttpStatus> deleteNote(@PathVariable("id") long id) {
        try {
            noteRepository.deleteById(id);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
