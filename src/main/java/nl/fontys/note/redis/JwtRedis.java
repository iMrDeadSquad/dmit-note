package nl.fontys.note.redis;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;

import java.time.Duration;

@Component
public class JwtRedis {
    @Autowired
    private RedisTemplate<String, String> redisTemplate;

    public void save(String jwt) {
        redisTemplate.opsForValue().set(jwt, jwt, Duration.ofHours(24));
    }

    public String get(String jwt) {
        return redisTemplate.opsForValue().get(jwt);
    }
}
